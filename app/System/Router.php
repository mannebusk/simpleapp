<?php
namespace System;

use \System\Db;

class Router
{
    protected $fileExtension = '.php';

    public function init()
    {
        $requestPath = explode('/', ltrim($_SERVER['REQUEST_URI'], '/'));

        Db::Instance();

        if (empty($requestPath[0])) {
            $this->renderHomePage();
            return $this;
        }

        if ($this->renderPage($requestPath[0])) {
            return $this;
        }

        $this->render404();
    }

    /**
     * renderHomePage 
     * 
     * @access public
     * @return $this
     */
    public function renderHomePage()
    {
        return $this->renderPage('index'); 
    }

    /**
     * renderPage 
     * 
     * @param mixed $name 
     * @access public
     * @return $this
     */
    public function renderPage($name)
    {
        $file = $_SERVER['DOCUMENT_ROOT'] . 'app/Page/' . $name . $this->fileExtension;
        if (file_exists($file)) {
            include($file);
            return true;
        }
        return false;
    }

    /**
     * render404 
     * 
     * @access public
     * @return $this
     */
    public function render404()
    {
        header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
        $this->renderPage('404'); 
        return $this;
    }
}
