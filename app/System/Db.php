<?php
namespace System;

class Db
{
    /**
     * Database Cinfiguration
     * 
     * @var array
     */
    private $_config;

    /**
     * PDO
     * 
     * @var PDO
     */
    private $pdo;

    /**
     * Class Instance
     *
     * @var Object
     */
    protected static $inst = null;

    /**
     * Call this method to get singleton
     *
     * @return Db
     */
    public static function Instance()
    {
        if (static::$inst === null) {
            static::$inst = new \System\Db;
            static::$inst->init();
        }
        return static::$inst;
    }

    /**
     * Private ctor so nobody else can instance it
     *
     * @return void
     */
    private function __construct()
    {
    }

    /**
     * Set up PDO database connection
     *
     * @return void
     */
    private function init()
    {
        require_once $_SERVER['DOCUMENT_ROOT'] . 'app/config.php';

        if (isset($appConf)) {
            $this->_config = $appConf['database'];
        }

        $dsn      = "mysql:host={$this->_config['host']};dbname={$this->_config['database']};";
        $options  = array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'");
        static::$inst->pdo = new \PDO($dsn, $this->_config['user'], $this->_config['password'], $options);

        static::$inst->pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);
    }

    /**
     * Fetch data from database
     * 
     * @param string    $query 
     * @param array     $params
     *
     * @static
     * @return array
     */
    public static function fetch($query, $params = [])
    {
        $query = static::$inst->pdo->prepare($query);

        foreach ($params as $param => $value) {
            $query->bindParam($param, $value);
        }

        $query->execute();

        return $query->fetchAll();
    }

    /**
     * Fetch only one row data from database
     * 
     * @param string    $query 
     * @param array     $params
     *
     * @static
     * @return Object
     */
    public static function fetchOne($query, $params = [])
    {
        $query = static::$inst->pdo->prepare($query);

        foreach ($params as $param => $value) {
            $query->bindParam($param, $value);
        }

        $query->execute();

        return $query->fetch();
    }

    /**
     * Run query on Database
     * (returns boolean, so not so useful for select statements)
     * 
     * @param string    $query 
     * @param array     $params
     *
     * @static
     * @return boolean
     */
    public static function query($query, $params = [])
    {
        $query = static::$inst->pdo->prepare($query);

        foreach ($params as $param => $value) {
            $query->bindParam($param, $value);
        }

        return $query->execute();
    }
}
