<?php
namespace Model;

use \System\Db;

class Example
{
    /**
     * Fetch all rows from the Example talbe
     *
     * @return array
     */
    public static function all()
    {
        return Db::fetch('SELECT * FROM Example');
    }

    /**
     * Get row by Id
     *
     * @return Object
     */
    public static function get($id)
    {
        /**
         * Second parameter is an array with PDO binds, where you bind a 
         * variable to a value in the SQL. This is the way to use varaibles in 
         * your SQL since PDO makes it impossible to do SQL injections.
         */
        return Db::fetchOne('SELECT * FROM Example WHERE id = :id', array(':id' => $id));
    }
}
