<h1>Example Page</h1>
<p>
    Lorem eligendi vitae accusamus quasi possimus unde qui quia officia rem saepe itaque pariatur obcaecati harum, obcaecati. Pariatur exercitationem temporibus ad dicta minima sit quisquam! Ipsum nulla sapiente ad eum.
</p>

<h3>All rows</h3>
<?php $allRows = \Model\Example::all(); ?>
<ul>
<?php foreach ($allRows as $row) : ?>
    <li><strong><?php echo $row->title ?></strong>: <?php echo $row->description ?></li> 
<?php endforeach; ?>
</ul>


<h3>Row with id 3</h3>
<?php $oneRow = \Model\Example::get(3); ?>
<ul>
    <li><strong><?php echo $oneRow->title ?></strong>: <?php echo $oneRow->description ?></li> 
</ul>

