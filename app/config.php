<?php
/**
 * Site Configuration File
 */
$appConf = array(
    /**
     * Database Configuration
     *
     */
    'database' => array(
        'host'      => '127.0.0.1',
        'user'      => 'root',
        'password'  => '',
        'database'  => 'simpleapp',
    ),
);
