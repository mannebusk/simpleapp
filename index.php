<?php
/**
 * Index file - Entry point of application
 *
 */
require 'vendor/autoload.php';

$router = new \System\Router;
$router->init();
